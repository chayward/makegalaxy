// makegalaxy configuration options. We should make these configure options.

//#define TESTING

// Scale V200, M200, R200, and CC with redshift
#define REDSHIFT_SCALING

// V200 is fixed with redshift, otherwise M200 is
#define V_SCALING

// use Hernquist 1993 Maxwellian assumption to set bulge/halo velocities
//#define MAXWELLIAN

// use eddington's method to calcualte f_e
#define DF_EDDINGTON

// use analytic H90 distribution function
//#define DF_H_MODEL

#define U4MOD
#define SCALEFIX
#define TJEOS
#define MOLECULAR_COOLING
